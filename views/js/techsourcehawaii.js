
/**
 * Common Javascript 
 *
 * Common js used site wide. Most of this will be used on every page because
 * a lot of it is for the header and navigation stuff. Dropdown menus, to 
 * top buttons, ect.
 */
var Common = new Object();

Common.btn = '';
Common.btnhtml = '';

Common.init = function() {
    Common.dropMenusDown();
    Common.openMobileNav();
    Common.highlightNavLink();
    Common.scrollHeader();
    Common.search();
    Common.alertEvents();
    Common.buttonClick();
}

// Drop menus
Common.dropMenusDown = function() {
    $('.nav ul').on('mouseover', '.dropdown-button', function() {
        if ($(window).width() >= 768) {
            $('.dropdown-menu', this).addClass('active');
            $('i', this).removeClass('fa-caret-down').addClass('fa-caret-up');
        }
    });
    $('.nav ul').on('mouseout', '.dropdown-button', function() {
        if ($(window).width() >= 768) {
            $('.dropdown-menu', this).removeClass('active');
            $('i', this).removeClass('fa-caret-up').addClass('fa-caret-down');
        }
    });  
    $('.nav ul').on('click', '.dropdown-button', function() {
        if ($(window).width() <= 768) {
            $('.dropdown-menu', this).toggleClass('active');
            $('i', this).toggleClass('fa-caret-up');
        }
    });
}

// Mobile navigation
Common.openMobileNav = function() {
    $('.header').on('click', '.menu-button', function() {
        $('.nav').toggleClass('active');
    });   
}

// Hightlight nav link
Common.highlightNavLink = function() {
    var pieces = window.location.pathname.split('/');
    var url = pieces[1];
    if (url == '') url = 'home';
    var nav_class = '.nav-link.' + url;
    $(nav_class).addClass('current');

    switch(url) {
        case 'computer-repair':
            $('.services').addClass('current');
            break;
        case 'help-desk':
            $('.services').addClass('current');
            break;
        case 'managed-backup':
            $('.services').addClass('current');
            break;
        case 'data-recovery':
            $('.services').addClass('current');
            break;
        case 'network-installations':
            $('.services').addClass('current');
            break;
        case 'phone-service':
            $('.services').addClass('current');
            break;
        case 'web-design':
            $('.services').addClass('current');
            break;
        default:
            ''
    } 
}

// Scroll header with page
Common.scrollHeader = function() {
    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('.header').addClass('fixed');
            $('.logo').addClass('fixed');
            $('.logo-text').addClass('fixed');
            $('.nav-main a').addClass('fixed');
            $('.dropdown-menu').addClass('fixed');
            $('.dropdown-menu a').addClass('fixed');
            $('.menu-button').addClass('fixed');
        } else {
            $('.header').removeClass('fixed');
            $('.logo').removeClass('fixed');
            $('.logo-text').removeClass('fixed');
            $('.nav-main a').removeClass('fixed');
            $('.dropdown-menu').removeClass('fixed');
            $('.dropdown-menu a').removeClass('fixed');
            $('.menu-button').removeClass('fixed');
        }
    });
}

Common.search = function() {
    $('.search button').click(function() {
        window.location.replace('/search/' + $('#search-term').val());
    });
    $('.search').keypress(function(e) {
        if (e.which == 13) { //Enter key pressed
            $('.search button').click(); //Trigger search button click event
            return false;
        }
    });
}

Common.showLoader = function(param) {
    $(param).prepend('<div class="loading"><div class="loading-spinner"><i class="fas fa-circle-notch fa-fw fa-spin"></i></div></div>');
}

Common.removeLoader = function() {
    $('.loading').remove();
}

Common.alertEvents = function() {
    $('body').on('click', '.alert-continue', function() {
        location.reload();
    });
    $('body').on('click', '.alert-close', function() {
        $(this).parent().remove();
    });
    $('body').on('click', '.alert-redirect', function() {
        location.replace('/admin/home');
    });
}

Common.buttonClick = function() {
    $('body').on('click', '.btn', function() {
        Common.btn = $(this);
        Common.btnhtml = Common.btn.html();
        $('.alert').remove();
        $('.alert-area').html('');
        if ($(this).hasClass('.no-load') == false) {
            $(this).html('<i class="fas fa-spinner fa-fw fa-spin"></i> loading...');
        }   
    });
}

Common.echo = function(string) {
    $('body').prepend(string);
}

$(document).ready(function() {

    $('body').on('click', '.alert-continue', function() {
        window.location.reload();
    });
    $('body').on('click', '.alert-close', function() {
        $(this).parent().remove();
    });
    $('body').on('click', '.alert-redirect', function() {
        window.location.replace('/home');
    });

});